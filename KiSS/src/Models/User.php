<?php

use \KiSS\Model;

/** @var Model $model */
$model = Model::getSharedInstance();

$properties = ['id', 'username', 'email', 'password', 'salt', 'status', 'role', 'register_time'];

$model->registerModel('User', 'users', $properties);