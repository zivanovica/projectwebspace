<?php

use \KiSS\Model;

/** @var Model $model */
$model = Model::getSharedInstance();

$properties = ['id', 'sender', 'recipient', 'title', 'message', 'is_read', 'deleted', 'timestamp'];

$model->registerModel('Message', 'messages', $properties);