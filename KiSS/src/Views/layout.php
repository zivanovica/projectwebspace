<!DOCTYPE html>
<html>
    <head>
        <title>KiSS Framework - PHP 7</title>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <div class="col-md-8">
                    <?php \KiSS\View::showBlock('header') ?>
                </div>
                <div class="col-md-offset-1 col-md-3">
                    <?php \KiSS\View::showBlock('navigation-block') ?>
                </div>
            </div>
            <div class="row"></div>
            <div class="col-md-2 sidebar">
                <?php \KiSS\View::showBlock('sidebar') ?>
            </div>
            <div class="col-md-offset-1 col-md-9">
                <?php \KiSS\View::showBlock('body') ?>
            </div>
            <?php echo $executionTime ?>
        </div>
    </body>
</html>