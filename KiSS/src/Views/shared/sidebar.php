<?php if (false === empty($links) && is_array($links)) : ?>
    <ul class="sidebar-links">
        <?php foreach ($links as $link) : ?>
            <li>
                <?php echo $link ?>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif ?>
