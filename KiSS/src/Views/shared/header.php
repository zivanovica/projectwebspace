<?php if (false === empty($links) && is_array($links)) : ?>
    <ul class="header-links">
        <?php foreach ($links as $link) : ?>
            <li>
                <a href="<?php echo $link['href'] ?>">
                    <?php echo $link['text'] ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif ?>
