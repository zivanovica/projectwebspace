<?php

use \KiSS\Middleware;

/** @var Middleware $middleware */
$middleware = Middleware::getSharedInstance();

$initTime = 0;

$middleware->onStart(function (Middleware $middleware) use (&$initTime) {
    $initTime = microtime(true);

    $middleware->next();
});

$middleware->onFinish(function (Middleware $middleware) use (&$initTime) {
    /** @var \KiSS\View $view */
    $view = \KiSS\View::getSharedInstance();

    $view->addBaseVar('executionTime', microtime(true) - $initTime);

    $middleware->next();
});