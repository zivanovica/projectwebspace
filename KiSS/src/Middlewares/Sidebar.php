<?php

use \KiSS\Middleware;

/** @var Middleware $middleware */
$middleware = Middleware::getSharedInstance();

$middleware->onStart(function (Middleware $middleware) {
    $view = \KiSS\View::getSharedInstance();

    $view->template('shared/sidebar.php', [
        'links' => ['www.google.com', 'www.facebook.com', 'www.twitter.com', 'www.youtube.com'],
        'archive' => []
    ], 'sidebar');

    $middleware->next();
});

$middleware->onFinish(function (Middleware $middleware) {
    $middleware->next();
});