<?php

use \KiSS\Middleware;
use \KiSS\Libraries\Request;
use \KiSS\Libraries\Database;

/** @var Middleware $middleware */
$middleware = Middleware::getSharedInstance();

$middleware->onActionStart('home.default', function (Middleware $middleware) {
    var_dump('EVO ME');
    $middleware->next();
});

$middleware->onActionStart('user.profile', function (Middleware $middleware) {
    var_dump('EVO ME');
    $middleware->next();
});

$middleware->onActionFinish('home.default', function (Middleware $middleware) {
    $middleware->next();
});

$middleware->onStart(function (Middleware $middleware) {
    var_dump('EVO ME OVDE');

    if (false === Request::getSharedInstance()->isGet()) {
        $middleware->next();

        return;
    }

    /** @var PDO $db */
    $db = Database::getSharedInstance()->getConnection();

    $query = $db->prepare('SELECT * FROM `links` WHERE `active` = 1;');

    if (false === $query->execute()) {
        throw new RuntimeException('Error fetching links.');
    }

    $view = \KiSS\View::getSharedInstance();

    $view->template(
        'shared/header.php',
        [
            'links' => $query->fetchAll(PDO::FETCH_ASSOC),
        ],
        'header'
    );

    $middleware->next();
});