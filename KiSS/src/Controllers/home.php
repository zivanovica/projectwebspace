<?php

use \KiSS\Controller;
use \KiSS\Libraries\Request;
use \KiSS\View;

$controller = Controller::getSharedInstance();

/** @var Controller $controller */
$controller->registerGet('default', function (Request $request) : View {
    $view = View::getSharedInstance();

    $data = ['email' => $request->get('email', null)];

    return $view->template('home/login.php', $data, 'body');
});

$controller->registerPost('login', function ($email, $password, Request $request) {
    return View::getSharedInstance();
}, ['email', 'password']);