<?php

/** @var \KiSS\Controller $controller */
$controller = \KiSS\Controller::getSharedInstance();

$controller->registerGet('location', function (\KiSS\Libraries\Request $request) {
    $view = \KiSS\View::getSharedInstance();

    $location = Location::getSharedInstance();

    return $view->template('admin/location.php', ['loc' => $location->getLocations()]);
});