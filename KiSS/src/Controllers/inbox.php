<?php

use \KiSS\Controller;
use \KiSS\Collection;
use \KiSS\Model;
use \KiSS\View;

/** @var Controller $controller */
$controller = Controller::getSharedInstance();

$controller->registerGet('messages', function () {
    /** @var Model $messages */
    $messages = Model::getNewInstance('Message');

    /** @var Collection $collection */
    $collection = $messages->fetchAll([['sender' => 1], ['recipient' => 1]]);

    /** @var View $view */
    $view = View::getSharedInstance();

    return $view->template('inbox/messages.php', ['messages' => $collection]);
});