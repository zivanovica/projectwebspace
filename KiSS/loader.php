<?php

// ============ FRAMEWORK - DON'T REMOVE ===============

require_once __DIR__ . '/KiSS/Extensions/Singleton.php';
require_once __DIR__. '/KiSS/Extensions/Runnable.php';

require_once __DIR__ . '/KiSS/Libraries/Database.php';
require_once __DIR__ . '/KiSS/Libraries/Request.php';


require_once __DIR__ . '/KiSS/Application.php';
require_once __DIR__ . '/KiSS/Model.php';
require_once __DIR__ . '/KiSS/Collection.php';
require_once __DIR__ . '/KiSS/View.php';
require_once __DIR__ . '/KiSS/Controller.php';
require_once __DIR__ . '/KiSS/Middleware.php';

// ============== END =================================

// ============= CUSTOM ===============================
require_once __DIR__ . '/src/Models/User.php';
require_once __DIR__ . '/src/Models/Message.php';