<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.29
 */

namespace KiSS\Extensions;

trait Singleton
{
    /** @var object */
    private static $instance;

    /**
     * @return object|$this
     */
    public static function getSharedInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = self::getInstance(func_get_args());
        }

        return static::$instance;
    }

    /**
     * @param array $arguments
     * @return $this|object
     */
    public static function getSharedInstanceArgs(array $arguments)
    {
        return self::getSharedInstance($arguments);
    }

    /**
     * @return object
     */
    public static function getNewInstance()
    {
        return self::getInstance(func_get_args());
    }

    /**
     * @param array $arguments
     * @return object
     */
    public static function getNewInstanceArgs(array $arguments)
    {
        return self::getInstance($arguments);
    }

    /**
     * @param array $arguments
     * @return object
     */
    private static function getInstance(array $arguments)
    {
        $reflection = new \ReflectionClass(static::class);

        $instance = $reflection->newInstanceWithoutConstructor();

        $constructor = $reflection->getConstructor();

        if (is_null($constructor)) {
            return $reflection->newInstanceArgs($arguments);
        }

        $constructor->setAccessible(true);

        $constructor->invokeArgs($instance, $arguments);

        return $instance;
    }
}