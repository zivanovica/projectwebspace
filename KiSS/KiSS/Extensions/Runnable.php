<?php
/**
 * Created by PhpStorm.
 * User: zvekete
 * Date: 26.6.2016.
 * Time: 05.41
 */

namespace KiSS\Extensions;


interface Runnable
{
    /**
     * @return bool
     */
    public function run() : bool;
}