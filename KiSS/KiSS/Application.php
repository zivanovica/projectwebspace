<?php
/**
 * Created by PhpStorm.
 * User: zvekete
 * Date: 26.6.2016.
 * Time: 05.42
 */

namespace KiSS;


use KiSS\Extensions\Runnable;
use KiSS\Extensions\Singleton;

class Application
{
    use Singleton;

    public function run(Runnable $runnable)
    {
        if (false === $runnable->run()) {
            throw new \RuntimeException('Application returned false response.');
        }
    }
}