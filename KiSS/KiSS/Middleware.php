<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 05.12
 */

namespace KiSS;


use KiSS\Extensions\Runnable;
use KiSS\Extensions\Singleton;

class Middleware implements Runnable
{
    use Singleton;

    const STARTUP_MOD = 0b00;
    const SHUTDOWN_MOD = 0b01;
    const ACTION_STARTUP_MOD = 0b10;
    const ACTION_SHUTDOWN_MOD = 0b11;

    const ERROR_INVALID_MIDDLEWARE = 0b0;

    const MIDDLEWARES_DIR = __DIR__ . '/../src/Middlewares';

    /** @var \Closure[] */
    private $_startupMiddlewares;

    /** @var \Closure[] */
    private $_shutdownMiddlewares;

    /** @var \Closure[] */
    private $_actionStartupMiddlewares;

    /** @var \Closure[] */
    private $_actionShutdownMiddlewares;

    /** @var int */
    private $_mod = Middleware::STARTUP_MOD;

    /** @var int */
    private $_running = 0;

    /** @var bool */
    private static $_booted = false;

    private function __construct()
    {
        if (false === is_readable(Middleware::MIDDLEWARES_DIR) || false === is_dir(Middleware::MIDDLEWARES_DIR)) {
            throw new \RuntimeException('Invalid middleware directory.');
        }

        $this->_startupMiddlewares = [];

        $this->_shutdownMiddlewares = [];

        $this->_actionStartupMiddlewares = [];

        $this->_actionShutdownMiddlewares = [];

    }

    /**
     * @param \Closure $callback
     * @return Middleware
     */
    public function onStart(\Closure $callback) : Middleware
    {
        $this->_startupMiddlewares[] = $callback;

        return $this;
    }

    /**
     * @param \Closure $callback
     * @return Middleware
     */
    public function onFinish(\Closure $callback) : Middleware
    {
        $this->_shutdownMiddlewares[] = $callback;

        return $this;
    }

    /**
     * @param string $action
     * @param \Closure $callback
     * @return Middleware
     */
    public function onActionStart(string $action, \Closure $callback) : Middleware
    {
        if (empty($this->_actionStartupMiddlewares[$action])) {
            $this->_actionStartupMiddlewares[$action] = [];
        }

        $this->_actionStartupMiddlewares[$action][] = $callback;

        return $this;
    }

    /**
     * @param string $action
     * @param \Closure $callback
     * @return Middleware
     */
    public function onActionFinish(string $action, \Closure $callback) : Middleware
    {
        if (empty($this->_actionShutdownMiddlewares[$action])) {
            $this->_actionShutdownMiddlewares[$action] = [];
        }

        $this->_actionShutdownMiddlewares[$action][] = $callback;

        return $this;
    }

    public function next()
    {
        $middleware = $this->getNextModMiddleware($this->_mod);

        if (is_callable($middleware)) {
            $middleware($this);
        }

        $this->_running--;
    }

    private function getNextModMiddleware(int $mod)
    {
        switch ($mod) {
            case Middleware::STARTUP_MOD:
                return next($this->_startupMiddlewares);
            case Middleware::SHUTDOWN_MOD:
                return next($this->_shutdownMiddlewares);
            case Middleware::ACTION_STARTUP_MOD:
                return next($this->_actionStartupMiddlewares);
            case Middleware::ACTION_SHUTDOWN_MOD:
                return next($this->_actionShutdownMiddlewares);
            default:
                throw new \RuntimeException(
                    "Invalid middleware mod {$this->_mod}", Middleware::ERROR_INVALID_MIDDLEWARE
                );
        }
    }


    /**
     * @return bool
     */
    public function run() : bool
    {
        $this->boot();

        $this->_running = count($this->_startupMiddlewares);

        $middleware = reset($this->_startupMiddlewares);

        if ($middleware) {
            $middleware($this);
        }

        return 0 === $this->_running;
    }

    /**
     * @return bool
     */
    public function end() : bool
    {
        $this->_mod = Middleware::SHUTDOWN_MOD;

        $this->_running = count($this->_shutdownMiddlewares);

        $middleware = reset($this->_shutdownMiddlewares);

        if ($middleware) {
            $middleware($this);
        }

        return 0 === $this->_running;
    }

    /**
     * @param string $action
     * @return bool
     */
    public function runAction(string $action) : bool
    {
        $this->boot();

        $this->_mod = Middleware::ACTION_STARTUP_MOD;

        $actionMiddlewares = empty($this->_actionStartupMiddlewares[$action]) ? null : $this->_actionStartupMiddlewares[$action];

        if (false === is_array($actionMiddlewares)) {
            return true;
        }

        $this->_running = count($this->_actionStartupMiddlewares[$action]);



        $middleware = reset($this->_actionStartupMiddlewares[$action]);

        if ($middleware) {
            $middleware($this);
        }

        return 0 === $this->_running;
    }

    /**
     * @param string $action
     * @return bool
     */
    public function endAction(string $action) : bool
    {
        $actionMiddlewares = empty($this->_actionShutdownMiddlewares[$action]) ? null : $this->_actionShutdownMiddlewares[$action];

        if (false === is_array($actionMiddlewares)) {
            return true;
        }

        $this->_mod = Middleware::ACTION_SHUTDOWN_MOD;

        $this->_running = count($this->_actionShutdownMiddlewares[$action]);

        $middleware = reset($this->_actionShutdownMiddlewares[$action]);

        if ($middleware) {
            $middleware($this);
        }

        return 0 === $this->_running;
    }

    private function boot()
    {
        if (self::$_booted) {
            return;
        }

        self::$_booted = true;

        $files = scandir(Middleware::MIDDLEWARES_DIR);

        foreach ($files as $file) {
            if (substr($file, -4) !== '.php') {
                continue;
            }

            require_once Middleware::MIDDLEWARES_DIR . "/{$file}";
        }
    }
}