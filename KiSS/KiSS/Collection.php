<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.31
 */

namespace KiSS;

use KiSS\Extensions\Singleton;

class Collection extends \ArrayObject
{
    use Singleton;
    
    const DEFAULT_PRIMARY_KEY = 'id';
    
    /** @var string */
    protected $primaryKey = Collection::DEFAULT_PRIMARY_KEY;

    /** @var string */
    protected $model;

    /**
     * @param Model $model
     */
    public function append($model)
    {
        if (false === $model instanceof Model) {
            throw new \RuntimeException('Invalid type.');
        }

        $this[$model->primaryKeyValue()] = $model;
    }


    /**
     * @param mixed $index
     * @param Model $newval
     */
    public function offsetSet($index, $newval)
    {
        if (false === $newval instanceof Model) {
            throw new \RuntimeException('Invalid type.');
        }

        if (null === $index) {
            $index = $newval->primaryKeyValue();
        }

        parent::offsetSet($index, $newval);
    }
}