<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.28
 */

namespace KiSS;

use KiSS\Extensions\Singleton;

class View
{
    use Singleton;

    const INVALID_LAYOUT = 0;

    const DEFAULT_LAYOUT_DIR = __DIR__ . '/../src/Views';

    const DEFAULT_LAYOUT = View::DEFAULT_LAYOUT_DIR . '/layout.php';

    const DEFAULT_BLOCK = 'body';

    /** @var string */
    private $_baseLayout = View::DEFAULT_LAYOUT;

    /** @var array */
    private static $_data = [];

    /** @var array */
    private static $_baseData = [];

    /** @var array */
    private static $_blocks = [];

    private function __construct()
    {
        $this->validateLayout($this->_baseLayout);
    }

    /**
     * @param string $layout
     * @param array $data
     * @param string $block
     * @return View
     */
    public function template(string $layout, array $data, string $block = View::DEFAULT_BLOCK) : View
    {
        $layout = View::DEFAULT_LAYOUT_DIR . "/{$layout}";

        $this->validateLayout($layout);

        if (false === isset(self::$_data[$block]) || false === is_array(self::$_data[$block])) {
            self::$_data[$block] = [];
        }

        self::$_data[$block] = array_merge(self::$_data[$block], $data);

        if (empty(self::$_blocks[$block])) {
            self::$_blocks[$block] = [];
        }

        self::$_blocks[$block] = $layout;

        return $this;
    }

    /**
     * @return bool
     */
    public function render() : bool
    {
        extract(self::$_baseData);

        try {
            require_once $this->_baseLayout;
        } catch (\Exception $e) {
            var_dump($e->getCode(), $e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param string $blockName
     * @param string $variableName
     * @param $value
     * @return View
     */
    public function addBlockVar(string $blockName, string $variableName, $value) : View
    {
        if (empty(self::$_blocks[$blockName])) {
            throw new \RuntimeException("Invalid block name {$blockName}.", View::INVALID_LAYOUT);
        }

        if (empty(self::$_data[$blockName])) {
            self::$_data[$blockName] = [];
        }

        self::$_data[$blockName][$variableName] = $value;

        return $this;
    }

    /**
     * @param string $variableName
     * @param $value
     * @return View
     */
    public function addBaseVar(string $variableName, $value) : View
    {
        self::$_baseData[$variableName] = $value;

        return $this;
    }

    /**
     * @param string $blockName
     */
    public static function showBlock(string $blockName)
    {
        if (empty(self::$_blocks[$blockName])) {
            return;
        }

        extract(self::$_baseData);

        if (false === empty(self::$_data[$blockName])) {
            extract(self::$_data[$blockName]);
        }

        require_once self::$_blocks[$blockName];
    }


    /**
     * @param string $layout
     * @throws \RuntimeException
     */
    private function validateLayout(string $layout)
    {
        if (false === is_readable($layout) || is_dir($layout)) {
            throw new \RuntimeException("Invalid base layout {$layout}", View::INVALID_LAYOUT);
        }
    }
}