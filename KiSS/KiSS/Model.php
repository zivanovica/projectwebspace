<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.28
 */

namespace KiSS;

use KiSS\Extensions\Singleton;
use KiSS\Libraries\Database;

class Model
{
    use Singleton;

    const DEFAULT_PRIMARY_KEY = 'id';

    private static $_models;

    /** @var string */
    private $_tableName;

    /** @var string */
    private $_primaryKey = Model::DEFAULT_PRIMARY_KEY;

    /** @var array */
    private $_originalData;

    /** @var array */
    private $_data;

    /** @var array */
    private $_properties;

    /** @var Collection */
    private static $_collection;

    /** @var \PDO */
    private $_db;

    private $_modelName;

    /**
     * Model constructor.
     * @param string $modelName
     * @param array|null $data
     */
    private function __construct(string $modelName = null, array $data = [])
    {
        if (null === $modelName) {
            return $this;
        }

        if (empty(self::$_models[$modelName])) {
            throw new \RuntimeException("Undefined model {$modelName}.");
        }

        $this->_db = Database::getSharedInstance()->getConnection();

        $this->_modelName = $modelName;

        $this->_originalData = $data;

        $this->_data = $data;

        $this->_tableName = self::$_models[$modelName]['table'];

        $this->_properties = array_flip(self::$_models[$modelName]['properties']);

        $this->_primaryKey = self::$_models[$modelName]['primaryKey'];

        if (false === empty($data)) {
            $this->fetch($data);
        }
    }


    /**
     * @return Collection
     */
    public function getCollection() : Collection
    {
        return empty(self::$_collection[$this->_modelName]) ? null : self::$_collection[$this->_modelName];
    }

    /**
     * @param string $modelName
     * @param string $tableName
     * @param array $properties
     * @param string $primaryKey
     * @return Model
     */
    public function registerModel(
        string $modelName, string $tableName, array $properties, string $primaryKey = Model::DEFAULT_PRIMARY_KEY
    ) : Model {
        if (empty($modelName)) {
            throw new \RuntimeException('Invalid model name.');
        }

        if (empty($tableName)) {
            throw new \RuntimeException('Invalid table name.');
        }

        self::$_models[$modelName] = [
            'table' => $tableName,
            'properties' => $properties,
            'primaryKey' => $primaryKey
        ];

        self::$_collection[$modelName] = Collection::getNewInstance();

        return $this;
    }

    /**
     * @return string
     */
    public function primaryKeyName() : string
    {
        return $this->_primaryKey;
    }

    /**
     * @return mixed|null
     */
    public function primaryKeyValue()
    {
        return $this->get($this->_primaryKey, null);
    }

    /**
     * @return bool
     */
    public function exists() : bool
    {
        return false === is_null($this->primaryKeyValue());
    }

    /**
     * @param string $property
     * @param mixed $default
     * @return mixed|null
     */
    public function get(string $property, $default = null)
    {
        return empty($this->_data[$property]) ? $default : $this->_data[$property];
    }

    /**
     * @param string $property
     * @param $value
     * @return Model
     */
    public function set(string $property, $value) : Model
    {
        if (false === isset($this->_properties[$property])) {
            throw new \RuntimeException("Undefined property {$property}.");
        }

        $this->_data[$property] = $value;

        return $this;
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function fetchAll(array $data) : Collection
    {
        $collection = Collection::getNewInstance();

        $orCriteria = [];

        $allParameters = [];

        foreach ($data as $criteria) {
            list ($criteriaString, $parameters) = $this->getWhereCriteriaWithParameters($criteria);

            $orCriteria[] = $criteriaString;
            $allParameters = array_merge($allParameters, $parameters);
        }

        $query = "SELECT * FROM `{$this->_tableName}` WHERE " . '(' . implode(') OR (', $orCriteria) . ')';

        $statement = $this->_db->prepare($query);

        if (false === $statement->execute($allParameters)) {
            throw new \RuntimeException("Error executing query {$query}");
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (is_array($results)) {
            return $this->hydrateAll($results);
        }

        return $collection;
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function hydrateAll(array $data) : Collection
    {
        $collection = Collection::getNewInstance();

        foreach ($data as $modelData) {
            /** @var Model $model */
            $model = Model::getNewInstance($this->_modelName);

            $model->hydrate($modelData);

            $collection[] = $model;
        }

        return $collection;
    }


    /**
     * @param array $data
     * @return Model
     */
    public function fetch(array $data) : Model
    {
        $id = empty($data[$this->primaryKeyName()]) ? null : $data[$this->primaryKeyName()];

        $result = null;

        if (is_null($id) || (false === is_null($id) && empty(self::$_collection[$this->_modelName][$id]))) {
            list($query, $parameters) = $this->getQueryWithParameters($data);

            $statement = $this->_db->prepare($query);

            if (false === $statement->execute($parameters)) {
                throw new \RuntimeException("Error executing query {$query}");
            }

            $result = $statement->fetch(\PDO::FETCH_ASSOC);
        } else if (false === is_null($id) && false === empty(self::$_collection[$this->_modelName][$id])) {
            $result = self::$_collection[$this->_modelName][$id];
        }

        if (is_array($result)) {
            $this->hydrate($result);
        }

        if ($this->exists() && is_array($result)) {
            self::$_collection[$this->_modelName][] = $this;
        }

        if ($result instanceof Model) {
            unset($this);
        }

        return $result instanceof Model ? $result : $this;
    }

    /**
     * @param array $data
     * @return Model
     */
    public function hydrate(array $data) : Model
    {
        foreach ($this->_properties as $property => $value) {
            unset($value);

            if (false === isset($data[$property])) {
                continue;
            }

            $this->_originalData[$property] = $data[$property];
        }

        $this->_data = $this->_originalData;

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    private function getQueryWithParameters(array $data) : array
    {
        $query = "SELECT * FROM `{$this->_tableName}` WHERE ";

        list ($criteria, $parameters) = $this->getWhereCriteriaWithParameters($data);

        $query .= $criteria;

        return [$query, $parameters];
    }

    /**
     * @param array $data
     * @return array
     */
    private function getWhereCriteriaWithParameters(array $data) : array
    {
        $criteria = [];

        foreach ($data as $field => $value) {
            $criteria[] = "`{$field}` = ?";
        }

        $criteriaString = implode(' AND ', $criteria);

        return [$criteriaString, array_values($data)];
    }

    /**
     * @return array
     */
    public function getAssocArray() : array
    {
        return $this->_data;
    }
}