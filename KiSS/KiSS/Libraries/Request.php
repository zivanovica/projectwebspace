<?php
/**
 * Created by PhpStorm.
 * User: zvekete
 * Date: 26.6.2016.
 * Time: 03.30
 */

namespace KiSS\Libraries;

use KiSS\Extensions\Singleton;

class Request
{
    use Singleton;

    const POST = 'POST';
    const GET = 'GET';
    const PUT = 'PUT';
    const HEAD = 'HEAD';
    const DELETE = 'DELETE';

    /** @var string */
    private $_method;

    /** @var array */
    private $_server;

    /** @var array */
    private $_get;

    /** @var array */
    private $_post;

    private function __construct()
    {
        $this->_server = filter_input_array(INPUT_SERVER);

        $this->_method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

        $this->_get = filter_input_array(INPUT_GET);

        $this->_post = filter_input_array(INPUT_POST);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function exists($name)
    {
        return false !== $this->get($name, false);
    }

    /**
     * @param string $name
     * @param string|int|float|double $default
     * @return mixed
     */
    public function get($name, $default = null)
    {
        $value = $default;

        switch ($this->_method) {
            case Request::POST:
            case Request::PUT:
                $value = false === isset($this->_post[$name]) ? $default : $this->_post[$name];
            default:
                if (is_null($value) || $value === $default) {
                    $value = false === isset($this->_get[$name]) ? $default : $this->_get[$name];
                }

        }

        return false === is_numeric($value) && is_string($value) && empty($value) ? $default : $value;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return $this->isMethod(Request::POST) || $this->isMethod(Request::PUT);
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return false === $this->isPost();
    }

    /**
     * @param string $method
     * @return bool
     *
     * @throws \RuntimeException
     */
    public function isMethod($method)
    {
        switch ($method) {
            case Request::POST:
            case Request::GET:
            case Request::DELETE:
            case Request::HEAD:
            case Request::PUT:
                return $this->_method === $method;
            default:
                throw new \RuntimeException("Invalid method {$method}.");
        }
    }
}