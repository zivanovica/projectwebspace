<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.29
 */

namespace KiSS\Libraries;


use KiSS\Extensions\Singleton;

class Database
{
    use Singleton;

    /** @var \PDO */
    private $db;

    /**
     * @param string $host
     * @param string $databaseName
     * @param string $user
     * @param string $password
     * @param int $port
     */
    private function __construct(string $host, string $databaseName, string $user, string $password, int $port = 3306)
    {
        $dsn = "mysql:host={$host};dbname={$databaseName};";

        $this->db = new \PDO($dsn, $user, $password, [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        ]);
    }

    /**
     * @return \PDO
     */
    public function getConnection() : \PDO
    {
        return $this->db;
    }
}