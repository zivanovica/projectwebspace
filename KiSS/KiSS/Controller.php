<?php

/**
 * Created by PhpStorm.
 * User: Aleksandar Zivanovic <coapsyfactor@gmail.com>
 * Date: 26.6.2016.
 * Time: 03.28
 */

namespace KiSS;

use KiSS\Extensions\Runnable;
use KiSS\Libraries\Request;
use KiSS\Extensions\Singleton;

class Controller implements Runnable
{
    use Singleton;

    const ERROR_MISSING_CONTROLLER = 0;

    const ERROR_INVALID_CONTROLLER = 1;

    const ERROR_INVALID_ACTION = 2;

    const ERROR_INVALID_CALLBACK = 3;

    const ERROR_DUPLICATE = 4;

    const ERROR_MISSING_PARAMETER = 5;

    /** @var array */
    private $_getRoutes = [];

    /** @var array */
    private $_postRoutes = [];
    
    /** @var Request */
    private $_request;

    /** @var Middleware */
    private $_middleware;

    /** @var string */
    private $_currentController;

    /**
     * Controller constructor.
     * @param Request $request
     * @param Middleware $middleware
     */
    private function __construct(Request $request, Middleware $middleware)
    {
        $this->_request = $request;
        $this->_middleware = $middleware;
    }

    /**
     * @param string $controller
     * @param \Closure|null $callback
     */
    public function registerController(string $controller, \Closure $callback = null)
    {

        $this->_currentController = $controller;

        if (empty($this->_postRoutes[$this->_currentController])) {
            $this->_postRoutes[$this->_currentController] = [];
        }

        if (empty($this->_getRoutes[$this->_currentController])) {
            $this->_getRoutes[$this->_currentController] = [];
        }

        if (is_callable($callback)) {
            $callback($this);
        }
    }

    /**
     * @param string $action
     * @param \Closure $callback
     * @param array $parameters
     *
     * @return Controller
     */
    public function registerPost(string $action, \Closure $callback, array $parameters = []) : Controller
    {
        $this->validateRegisterInput($action, $this->_postRoutes);

        $this->_postRoutes[$this->_currentController][$action] = ['callback' => $callback, 'parameters' => $parameters];

        return $this;
    }

    /**
     * @param string $action
     * @param \Closure $callback
     * @param array $parameters
     *
     * @return Controller
     */
    public function registerGet(string $action, \Closure $callback, array $parameters = []) : Controller
    {
        $this->validateRegisterInput($action, $this->_getRoutes);

        $this->_getRoutes[$this->_currentController][$action] = ['callback' => $callback, 'parameters' => $parameters];

        return $this;
    }


    public function run() : bool
    {
        $data = $this->getParsedActionData($this->_request->get('action', 'home.default'));

        if (is_null($data['controller'])) {
            throw new \RuntimeException('Missing controller', Controller::ERROR_MISSING_CONTROLLER);
        }

        $controllerPath = __DIR__ . "/../src/Controllers/{$data['controller']}.php";

        if (is_readable($controllerPath) && false === is_dir($controllerPath)) {
            $this->registerController($data['controller'], function () use ($controllerPath) {
                require_once $controllerPath;
            });
        }

        /** @var array $controllersCollection */
        $controllersCollection = $this->_request->isGet() ? $this->_getRoutes : $this->_postRoutes;

        $this->validateRouteController($controllersCollection, $data['controller'], $data['action']);

        $routeController = $controllersCollection[$data['controller']][$data['action']];

        if ($this->_middleware->run()) {
            return $this->execute("{$data['controller']}.{$data['action']}", $routeController['callback'], $routeController['parameters']);
        }

        return false;
    }

    /**
     * @param string $action
     * @return array
     */
    private function getParsedActionData(string $action) : array
    {
        $data = explode('.', $action);

        $controller = array_shift($data);

        $controller = strtolower($controller);

        $action = implode('.', $data);

        $action = empty($action) ? 'default' : $action;

        return ['controller' => $controller, 'action' => $action];
    }


    /**
     * @param string $action
     * @param \Closure $callback
     * @param array $requiredParameters
     * @return bool
     */
    private function execute(string $action, \Closure $callback, array $requiredParameters = []) : bool
    {
        if (false === $this->_middleware->runAction($action)) {
            return false;
        }

        $parameters = [$this->_request];

        /** @var View $view */
        if (false === empty($requiredParameters)) {
            $parameters = $this->getAndValidateRequiredParameters($requiredParameters);
        }

        $view = call_user_func_array($callback, $parameters);

        if ($this->_middleware->endAction($action) && $this->_middleware->end()) {
            $view->render();

            return true;
        }

        return false;
    }

    /**
     * @param array $requiredParameters
     * @return array
     */
    private function getAndValidateRequiredParameters(array $requiredParameters) : array
    {
        $parameters = [];

        foreach ($requiredParameters as $parameter) {
            if (false === $this->_request->exists($parameter)) {
                throw new \RuntimeException(
                    "Missing required parameter {$parameter}", Controller::ERROR_MISSING_PARAMETER
                );
            }

            $parameters[] = $this->_request->get($parameter, null);
        }

        $parameters[] = $this->_request;

        return $parameters;
    }

    /**
     * @param array $controllersCollection
     * @param string $controller
     * @param string $action
     *
     * @throws \RuntimeException
     */
    private function validateRouteController(array $controllersCollection, string $controller, string $action)
    {
        if (empty($controllersCollection[$controller])) {
            throw new \RuntimeException("Invalid controller {$controller}.", Controller::ERROR_INVALID_CONTROLLER);
        }

        /** @var array $routeController */
        $routeController = $controllersCollection[$controller];

        if (empty($routeController[$action])) {
            throw new \RuntimeException(
                "Invalid action {$action} in controller {$controller}.", Controller::ERROR_INVALID_ACTION
            );
        }

        /** @var array $routeController */
        $routeAction = $routeController[$action];

        if (empty($routeAction['callback']) || false === is_callable($routeAction['callback'])) {
            throw new \RuntimeException(
                "Missing callback for {$controller} action {$action}.",
                Controller::ERROR_INVALID_CALLBACK
            );
        }

        if (empty($routeAction['parameters'])) {
            $routeAction['parameters'] = [];
        }
    }

    /**
     * @param $action
     * @param array $registeredCallbacks
     *
     * @throws \RuntimeException
     */
    private function validateRegisterInput(string $action, array $registeredCallbacks)
    {
        if (empty($this->_currentController)) {
            throw new \RuntimeException('Controller name must be defined', Controller::ERROR_MISSING_CONTROLLER);
        }

        if (empty($action)) {
            throw new \RuntimeException('Missing action for controller', Controller::ERROR_INVALID_ACTION);
        }

        if (false === empty($registeredCallbacks[$this->_currentController][$action])) {
            throw new \RuntimeException(
                "Action {$action} in controller {$this->_currentController} already registered.",
                Controller::ERROR_DUPLICATE
            );
        }
    }
}