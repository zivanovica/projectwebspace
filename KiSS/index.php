<?php

require_once __DIR__ . '/loader.php';

use \KiSS\Controller;
use \KiSS\Libraries\Request;
use \KiSS\Application;
use \KiSS\Middleware;
use \KiSS\Libraries\Database;

Database::getSharedInstance('localhost', 'kiss_example', 'root', '');

$app = Application::getNewInstance();

$app->run(
    Controller::getSharedInstance(
        Request::getSharedInstance(),
        Middleware::getSharedInstance()
    )
);